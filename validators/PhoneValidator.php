<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   02.09.2017
 * 
 * Example custom regex validator
 * How to use:
 * 
 * class ExampleModel extends Model
 * {
 *     public function rules() {
 *         return [
 *             ['phone', PhoneValidator::class],
 *             // ...
 *         ];
 *     }
 * }
 */

namespace app\validators;
use yii\validators\RegularExpressionValidator;

class PhoneValidator extends RegularExpressionValidator
{
    public $pattern = '^\(?\+?([0-9]{1,4})\)?[-\. ]?(\d{10})$';
}
