<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   02.09.2017
 */

namespace app\validators;
use Yii;
use yii\validators\Validator;

class AuthManagerRoleValidator extends Validator
{
    protected function validateValue($value) {
        if (Yii::$app->authManager->getRole($value)) {
            return NULL;
        }
        return [$this->message, []];
    }
}
