// navigation
$(function() {
    var $side_menu = $('#side-menu');
    if (!$side_menu.length) {
        return;
    }
    var $sub_menus = $side_menu.find('.nav-second-level'),
        $items = $side_menu.find('a');
    // open submenu
    $sub_menus.each(function() {
        var $t = $(this), $items = $t.find('a');
        if ($items.hasClass('active')) {
            $t.addClass('collapse');
            $t.addClass('in');
            return false;
        }
    });
    // activate automatically
    if (!$items.filter('.active').length) {
        var current_url = window.location.href;
        var $item = $items.filter(function() {
            return this.href == current_url || current_url.indexOf(this.href) == 0;
        }).addClass('active').parent().parent().addClass('in').parent();
        if ($item.is('li')) {
            $item.addClass('active');
        }
    }
    // initialize collapse
    $side_menu.metisMenu();
});

//Loads the correct sidebar on window load,
//collapses the sidebar on window resize.
// Sets the min-height of #page-wrapper to window size
$(window).on('load resize', function() {
    topOffset = 50;
    width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
    if (width < 768) {
        $('div.navbar-collapse').addClass('collapse');
        topOffset = 100; // 2-row-menu
    } else {
        $('div.navbar-collapse').removeClass('collapse');
    }

    height = ((this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height) - 1;
    height = height - topOffset;
    if (height < 1) height = 1;
    if (height > topOffset) {
        $("#page-wrapper").css("min-height", (height) + "px");
    }
});