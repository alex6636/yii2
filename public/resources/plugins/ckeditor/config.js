/**
 * @license Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here.
	// For complete reference see:
	// http://docs.ckeditor.com/#!/api/CKEDITOR.config

    config.filebrowserBrowseUrl = '/resources/plugins/kcfinder/browse.php?type=files';
    config.filebrowserImageBrowseUrl = '/resources/plugins/kcfinder/browse.php?type=images';
    config.filebrowserFlashBrowseUrl='/resources/plugins/kcfinder/browse.php?type=flash';
    config.filebrowserUploadUrl='/resources/plugins/kcfinder/upload.php?type=files';
    config.filebrowserImageUploadUrl='/resources/plugins/kcfinder/upload.php?type=images';
    config.filebrowserFlashUploadUrl='/resources/plugins/kcfinder/upload.php?type=flash';

    config.extraPlugins='mediaembed';

	// The toolbar groups arrangement, optimized for two toolbar rows.
	config.toolbarGroups = [
		{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
		{ name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
		{ name: 'links' },
		{ name: 'insert' },
		{ name: 'forms' },
		{ name: 'tools' },
		{ name: 'document',	   groups: [ 'mode', 'document', 'doctools' ] },
		{ name: 'others' },
        { name: 'mediaembed'},
		'/',
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
		{ name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
		{ name: 'styles' },
		{ name: 'colors' },
		{ name: 'about' }
	];

	// Remove some buttons provided by the standard plugins, which are
	// not needed in the Standard(s) toolbar.
	config.removeButtons = 'Underline,Subscript,Superscript';

	// Set the most common block elements.
	config.format_tags = 'p;h1;h2;h3;h4;h5;h6;pre';

	// Simplify the dialog windows.
	config.removeDialogTabs = 'image:advanced;link:advanced';

    // при нажатии enter добавляем br
    config.enterMode = CKEDITOR.ENTER_BR;

    config.mediaembed_outerClass = 'embed_outer';
};
