<?php
namespace app\models;
use app\modules\admin\models\CrudModelSorted;
use app\models\query\ArticleQuery;
use alexs\yii2sluggable\Sluggable;
use alexs\yii2fileable\Imageable;
use alexs\yii2fileable\Fileable;
use alexs\yii2manytomany\ManyToMany;

/**
 * @property int $id
 * @property string $slug
 * @property string $title
 * @property string $text
 * @property string $image
 * @property string $file 
 * @property int $public
 * @property int $pos
 * @property string $short_text
 * @property int $article_category_id
 */

class Article extends CrudModelSorted
{
    public $tag_id = [];
    public $gallery_image = [];
    
    public function rules() {
        return [
            [['title', 'text', 'short_text'], 'filter', 'filter'=>'trim'],
            [['title'], 'required'],
            [['text'], 'string'],
            [['image'], 'image', 'extensions'=>['jpg', 'jpeg', 'png', 'gif']],
            [['file'], 'file', 'extensions'=>'pdf'],
            [['tag_id'], 'each', 'rule'=>['integer']],
            [['public'], 'boolean'],
            [['pos', 'article_category_id'], 'integer'],
            ['gallery_image', 'each', 'rule'=>['integer']],
        ];
    }

    public function behaviors() {
        return [
            [
                'class'=>Sluggable::class,
            ],
            [
                'class'=>Imageable::class,
                'upload_dir'=>static::getUploadDir(),
                'thumbnails'=>[
                    ['width'=>600, 'height'=>600],
                    ['subdir'=>'thumb', 'width'=>300, 'height'=>300],
                    ['subdir'=>'thumb2', 'width'=>150, 'height'=>150, 'crop'=>true],
                ],
            ],
            [
                'class'=>Fileable::class,
                'upload_dir'=>static::getUploadDir(),
            ],
            [
                'class'=>ManyToMany::class,
                'relations'=>[
                    'article_tag'=>[
                        'article_id',
                        'tag_id',
                    ],
                ],
            ],
        ];
    }

    public function attributeLabels() {
        return [
            'title'=>\Yii::t('app', 'Title'),
            'text'=>\Yii::t('app', 'Text'),
            'public'=>\Yii::t('app', 'Public'),
            'file'=>'PDF',
            'image'=>\Yii::t('app', 'Image'),
            'tag_id'=>\Yii::t('app', 'Tags'),
            'short_text'=>\Yii::t('app', 'Short text'),
            'article_category_id'=>\Yii::t('app', 'Category'),
        ];
    }

    public function afterSave($insert, $changedAttributes) {
        if ($insert) {
            // attach gallery images
            if (!empty($this->gallery_image)) {
                foreach ($this->gallery_image as $gallery_image_id) {
                    /** @var ArticleGallery $ArticleGallery */
                    if (!$ArticleGallery = ArticleGallery::findOne($gallery_image_id)) {
                        continue;
                    }
                    $ArticleGallery->{$ArticleGallery->getGalleryRelationAttribute()} = $this->id;
                    $ArticleGallery->save(false);
                }
            }
        }
        parent::afterSave($insert, $changedAttributes);
    }

    public static function find() {
        return new ArticleQuery(static::class);
    }
    
    public function isPublished() {
        return $this->public == 1;
    }
}
