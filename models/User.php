<?php
namespace app\models;
use app\modules\admin\models\CrudModel;
use app\validators\AuthManagerRoleValidator;
use Yii;
use yii\web\IdentityInterface;

/**
 * @property int $id
 * @property string $role
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $auth_key
 * @property string $access_token
 * @property string $forgot_key
 */

class User extends CrudModel implements IdentityInterface
{
    const SCENARIO_ADD   = 'add';
    const SCENARIO_EDIT  = 'edit';
    
    public function rules() {
        return [
            [['name', 'email', 'password', 'role'], 'filter', 'filter'=>'trim'],
            [['name', 'email', 'role'], 'required'],
            ['password', 'required', 'on'=>[static::SCENARIO_ADD]],
            ['email', 'email'],
            ['email', 'unique', 'message'=>\Yii::t('app', 'Entered email is already exists')],
            ['role', AuthManagerRoleValidator::class, 'message'=>\Yii::t('app','Role is invalid')],
        ];
    }

    public function scenarios() {
        return [
            static::SCENARIO_ADD =>['name', 'email', 'password', 'role'],
            static::SCENARIO_EDIT=>['name', 'email', 'password', 'role'],
        ];
    }

    public function attributeLabels() {
        return [
            'name'=>Yii::t('app', 'Name'),
            'password'=>Yii::t('app', 'Password'),
            'role'=>Yii::t('app', 'Role'),
        ];
    }

    /**
     * Finds an identity by the given token.
     *
     * @param string $token the token to be looked for
     * @param mixed $type
     * @return IdentityInterface|null the identity object that matches the given token.
     */
    public static function findIdentityByAccessToken($token, $type = null) {
        return self::findOne(['access_token'=>$token]);
    }

    /**
     * Finds an identity by the given ID.
     *
     * @param string|integer $id the ID to be looked for
     * @return IdentityInterface|null the identity object that matches the given ID.
     */
    public static function findIdentity($id) {
        return self::findOne($id);
    }

    public function getId() {
        return $this->id;
    }

    public function getAuthKey() {
        return $this->auth_key;
    }

    /**
     * @param string $authKey
     * @return boolean if auth key is valid for current user
     */
    public function validateAuthKey($authKey) {
        return $this->getAuthKey() === $authKey;
    }
    
    public function beforeSave($insert) {
        if (static::SCENARIO_ADD === $this->scenario) {
            $this->auth_key     = Yii::$app->getSecurity()->generateRandomString();
            $this->access_token = Yii::$app->getSecurity()->generateRandomString();
            $this->password     = $this->getPasswordHash($this->password);
        } elseif (static::SCENARIO_EDIT === $this->scenario) {
            if (!empty($this->password)) {
                // generate a new hash
                $this->password = $this->getPasswordHash($this->password);
            } else {
                // use an old hash
                $this->password = $this->getOldAttribute('password');
            }
        }
        return parent::beforeSave($insert);
    }

    public function beforeDelete() {
        if (Yii::$app->user->id === $this->id) {
            return false;
        }
        return parent::beforeDelete();
    }

    public function login($duration = 3600) {
        if (Yii::$app->authManager->checkAccess($this->id, $this->role)) {
            return Yii::$app->user->login($this, $duration);
        }
        return false;
    }
    
    /**
     * @param string $password entered user password
     * @return boolean if entered password and saved password match
     */
    public function validatePassword($password) {
        $Security = Yii::$app->getSecurity();
        return $Security->validatePassword($password, $this->password);
    }

    public function getPasswordHash($password) {
        return Yii::$app->getSecurity()->generatePasswordHash($password);
    }

    public function updatePassword($password) {
        $hash = $this->getPasswordHash($password);
        $this->password = $hash;
        $this->save(false);
        return $hash;
    }

    public function updateForgotKey($forgot_key) {
        $this->forgot_key = $forgot_key;
        $this->save(false);
        return $forgot_key;
    }
}
