<?php
namespace app\models;
use app\modules\admin\models\CrudModel;

/**
 * @property int $id
 * @property string $title
 * @property string $text
 */

class Page extends CrudModel
{
    public function rules() {
        return [
            [['title', 'text'], 'filter', 'filter'=>'trim'],
        ];
    }

    public function attributeLabels() {
        return [
            'title'=>\Yii::t('app', 'Title'),
            'text' =>\Yii::t('app', 'Text'),
        ];
    }
}
