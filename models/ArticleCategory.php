<?php
namespace app\models;
use app\modules\admin\models\CrudModel;
use yii\db\ActiveQuery;

/**
 * @property int $id
 * @property string $name
 */

class ArticleCategory extends CrudModel
{
    public function rules() {
        return [
            [['name'], 'filter', 'filter'=>'trim'],
            [['name'], 'required'],
        ];
    }

    public function attributeLabels() {
        return [
            'name'=>\Yii::t('app', 'Category'),
        ];
    }

    /**
     * Return items list
     *
     * @return ActiveQuery
     */
    public static function findItems() {
        return static::find()->orderBy('name');
    }

    /**
     * The name for use on HTML-pages, messages, etc.
     * Make sense to redeclare for models with complex names
     * 
     * @return string
     */
    public static function getHumanModelName() {
        return 'Category';
    }
}
