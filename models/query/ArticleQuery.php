<?php
namespace app\models\query;
use yii\db\ActiveQuery;

class ArticleQuery extends ActiveQuery
{
    public function published() {
        return $this->andWhere(['public'=>1]);
    }

    public function forTag($tag_id) {
        return $this->andWhere(
            'id IN (SELECT article_id FROM article_tag WHERE tag_id=:tag_id)',
            ['tag_id'=>$tag_id]
        );
    }
}
