<?php
namespace app\models;

class ArticleGallery extends \alexs\alexgallery\models\Gallery
{
    /**
     * @inheritDoc
     */
    public static function getGalleryRelationAttribute() {
        return 'article_id';
    }
}
