<?php
namespace app\models;
use app\modules\admin\models\CrudModel;
use alexs\yii2sluggable\Sluggable;
use yii\db\ActiveQuery;

/**
 * @property int $id
 * @property string $slug
 * @property string $name
 */

class Tag extends CrudModel
{
    public function rules() {
        return [
            [['name'], 'filter', 'filter'=>'trim'],
            [['name'], 'required'],
        ];
    }

    public function behaviors() {
        return [
            [
                'class'=>Sluggable::class,
                'attribute_from'=>'name',
            ],
        ];
    }

    public function attributeLabels() {
        return [
            'name'=>\Yii::t('app', 'Tag'),
        ];
    }

    /**
     * Return items list
     *
     * @return ActiveQuery
     */
    public static function findItems() {
        return static::find()->orderBy('name');
    }
}
