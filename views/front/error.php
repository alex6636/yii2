<?php
/** @var yii\web\View $this */
/** @var $name string */
/** @var $message string */

$this->title = $name;
?>

<?php $this->beginBlock('content'); ?>
<div class="error">
    <h1 class="code"><?=$name?></h1>
    <h2 class="message"><?=$message?></h2>
</div>
<?php $this->endBlock(); ?>
