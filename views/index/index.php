<?php
use app\widgets\MessageWidget;
use yii\helpers\Html;

/** @var yii\web\View $this */

$this->title = 'Home page';
?>

<?php $this->beginBlock('content'); ?>

<?=MessageWidget::widget(['message'=>$this->title])?>
<small><?=Html::encode(Yii::$app->name)?></small>

<?php $this->endBlock(); ?>
