<?php
use yii\helpers\Html;

/** @var yii\web\View $this */
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?=Yii::$app->language;?>">
<head>
    <meta charset="<?=Yii::$app->charset;?>"/>
    <?=Html::csrfMetaTags()?>
    <title><?=Html::encode($this->title);?></title>
    <?php $this->head() ?>
</head>
<body>
    <?php $this->beginBody() ?>
    
    <?=isset($this->blocks['content']) ? $this->blocks['content'] : '';?>

    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
