<?php
use yii\db\Schema;
use yii\db\Migration;

/**
 * Handles the creation of table `{{%article_type}}`.
 */
class m190524_203928_create_article_type_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%article_type}}', [
            'id'  => $this->primaryKey(),
            'name'=> Schema::TYPE_STRING,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%article_type}}');
    }
}
