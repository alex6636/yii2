<?php

use yii\db\Migration;

/**
 * Class m190215_122859_article_text_type_text
 */
class m190215_122859_article_text_type_text extends Migration
{
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->alterColumn('article', 'text', $this->text());
    }

    public function down()
    {
        $this->alterColumn('article', 'text', $this->string());
    }
}
