<?php
use yii\db\Schema;
use yii\db\Migration;

/**
 * Handles adding article_category_id to table `{{%article}}`.
 */
class m190525_160133_add_article_category_id_column_to_article_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(
            '{{%article}}',
            'article_category_id',
            Schema::TYPE_INTEGER . ' DEFAULT NULL AFTER `text`'
        );
        $this->addForeignKey(
            'fk-article_category_id',
            '{{%article}}',
            'article_category_id',
            'article_category',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-article_category_id',
            '{{%article}}'
        );
        $this->dropColumn(
            '{{%article}}',
            'article_category_id'
        );
    }
}
