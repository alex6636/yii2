<?php

use yii\db\Migration;

/**
 * Class m190215_121609_article_short_text
 */
class m190215_121609_article_short_text extends Migration
{
    public function up()
    {
        $this->addColumn('article', 'short_text', $this->string());
    }

    public function down()
    {
        $this->dropColumn('article', 'short_text');
    }
}
