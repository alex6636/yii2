<?php
use yii\db\Schema;
use yii\db\Migration;

/**
 * Handles adding article_type_id to table `{{%article}}`.
 */
class m190524_204501_add_article_type_id_column_to_article_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(
            '{{%article}}',
            'article_type_id',
            Schema::TYPE_INTEGER . ' DEFAULT NULL AFTER `text`'
        );
        $this->addForeignKey(
            'fk-article_type_id',
            '{{%article}}',
            'article_type_id',
            'article_type',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-article_type_id',
            '{{%article}}'
        );
        $this->dropColumn(
            '{{%article}}',
            'article_type_id'
        );
    }
}
