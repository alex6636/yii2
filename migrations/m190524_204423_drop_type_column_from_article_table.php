<?php
use yii\db\Schema;
use yii\db\Migration;

/**
 * Handles dropping type from table `{{%article}}`.
 */
class m190524_204423_drop_type_column_from_article_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%article}}','type');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('{{%article}}', 'type', $this->tinyInteger());
    }
}
