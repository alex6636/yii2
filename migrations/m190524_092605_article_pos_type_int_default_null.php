<?php

use yii\db\Migration;

/**
 * Class m190524_092605_article_pos_type_int_default_null
 */
class m190524_092605_article_pos_type_int_default_null extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('article', 'pos', $this->integer()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('article', 'pos', $this->integer()->notNull());
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190524_092605_article_pos_type_int_default_null cannot be reverted.\n";

        return false;
    }
    */
}
