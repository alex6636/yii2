<?php

use yii\db\Migration;

/**
 * Handles adding type to table `{{%article}}`.
 */
class m190524_142426_add_type_column_to_article_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('article', 'type', $this->tinyInteger());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('article', 'type');
    }
}
