<?php
use yii\db\Migration;
use yii\db\Schema;

/**
 * Handles the creation of table `{{%gallery}}`.
 */
class m190612_081144_create_gallery_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%gallery}}', [
            'id'=>$this->primaryKey(),
            'image'=>Schema::TYPE_STRING,
            'pos'=>Schema::TYPE_INTEGER,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%gallery}}');
    }
}
