<?php
use yii\db\Schema;
use yii\db\Migration;

/**
 * Handles the dropping of table `{{%article_type_id_column_form_article}}`.
 */
class m190525_160347_drop_article_type_id_column_form_article_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey(
            'fk-article_type_id',
            '{{%article}}'
        );
        $this->dropColumn(
            '{{%article}}',
            'article_type_id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn(
            '{{%article}}',
            'article_type_id',
            Schema::TYPE_INTEGER . ' DEFAULT NULL AFTER `text`'
        );
        $this->addForeignKey(
            'fk-article_type_id',
            '{{%article}}',
            'article_type_id',
            'article_type',
            'id',
            'SET NULL'
        );
    }
}
