<?php
use yii\db\Migration;
use yii\db\Schema;

/**
 * Handles the creation of table `{{%article_gallery}}`.
 */
class m190612_102528_create_article_gallery_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%article_gallery}}', [
            'id'=>$this->primaryKey(),
            'article_id'=>Schema::TYPE_INTEGER . ' DEFAULT NULL',
            'image'=>Schema::TYPE_STRING,
            'pos'=>Schema::TYPE_INTEGER,
        ]);
        $this->addForeignKey(
            'fk-article_gallery_article_id',
            '{{%article_gallery}}',
            'article_id',
            'article',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-article_gallery_article_id',
            '{{%article_gallery}}'
        );
        $this->dropTable('{{%article_gallery}}');
    }
}
