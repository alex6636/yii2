<?php
use yii\db\Schema;
use yii\db\Migration;

/**
 * Handles the dropping of table `{{%article_type}}`.
 */
class m190525_160604_drop_article_type_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropTable('{{%article_type}}');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->createTable('{{%article_type}}', [
            'id'  => $this->primaryKey(),
            'name'=> Schema::TYPE_STRING,
        ]);
    }
}
