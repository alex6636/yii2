SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;


CREATE TABLE `article` (
  `id` int(4) NOT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `text` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `public` tinyint(1) NOT NULL DEFAULT '0',
  `pos` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB;

INSERT INTO `article` (`id`, `slug`, `title`, `text`, `image`, `file`, `public`, `pos`) VALUES
(8, 'first-article', 'First article', 'First article contents', '9f0512da355c1a0.jpg', 'c5c6f68276a270b.pdf', 1, 1),
(9, 'second', 'Second', '', '', '', 0, 2);

CREATE TABLE `article_tag` (
  `article_id` int(4) NOT NULL,
  `tag_id` int(4) NOT NULL
) ENGINE=InnoDB;

INSERT INTO `article_tag` (`article_id`, `tag_id`) VALUES
(8, 5),
(9, 5),
(9, 6);

CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB;

INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('administrator', 1, 1534499150),
('blog editor', 2, 1534499380),
('page editor', 3, 1534499373);

CREATE TABLE `auth_item` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `rule_name` varchar(64) DEFAULT NULL,
  `data` text,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB;

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('administrator', 1, 'Administrator', NULL, NULL, 1416910483, 1534499302),
('admins', 2, NULL, NULL, NULL, 1469701123, 1469701123),
('blog', 2, NULL, NULL, NULL, 1469701134, 1469701134),
('blog editor', 1, NULL, NULL, NULL, 1469701460, 1534499317),
('page editor', 1, NULL, NULL, NULL, 1534499335, 1534499335),
('pages', 2, NULL, NULL, NULL, 1534499280, 1534499280);

CREATE TABLE `auth_item_child` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL
) ENGINE=InnoDB;

INSERT INTO `auth_item_child` (`parent`, `child`) VALUES
('administrator', 'admins'),
('administrator', 'blog'),
('blog editor', 'blog'),
('administrator', 'pages'),
('page editor', 'pages');

CREATE TABLE `auth_rule` (
  `name` varchar(64) NOT NULL,
  `data` text,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB;

CREATE TABLE `page` (
  `id` int(4) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `text` text
) ENGINE=InnoDB;

INSERT INTO `page` (`id`, `title`, `text`) VALUES
(1, 'Home', 'Home page contents'),
(2, 'Contact', 'Contact page contents');

CREATE TABLE `tag` (
  `id` int(4) NOT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB;

INSERT INTO `tag` (`id`, `slug`, `name`) VALUES
(5, 'lorem', 'Lorem'),
(6, 'ipsum', 'Ipsum');

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `role` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `auth_key` varchar(255) NOT NULL,
  `access_token` varchar(255) NOT NULL,
  `forgot_key` varchar(255) DEFAULT NULL
) ENGINE=InnoDB;

INSERT INTO `user` (`id`, `role`, `name`, `email`, `password`, `auth_key`, `access_token`, `forgot_key`) VALUES
(1, 'administrator', 'Admin', 'admin@admin.com', '$2y$13$TZCv3EvNdz4nnsVIsaddHOrZznZ.ftqSpI4QJvEgFVtH3dEM4UudO', 'QpDS1rDP10ARz5tSkrXhVQiPaV-ebdGK', 'JH2fcPsOruHCUXh-htoprA-3qlvuYDGR', 'DJjvdZpNQIKiLzSp528zlWY5Q2_xnZaM'),
(2, 'blog editor', 'Blog editor', 'blogeditor@editor.com', '$2y$13$DO.hVCONjWFbbFDRx3dkLeJUZd59JLuMeGbStvkpmBJZ/4eDsdJJK', 'siYKs4eidtzGD42smf497NzlVffLpM3S', 'rbrwn-K7FhTIWVcfVrhCmN7k-qd3MSgb', NULL),
(3, 'page editor', 'Page editor', 'pageeditor@editor.com', '$2y$13$5KWXdghNfVTiY/HeqsMl7.Z2VnX8tZKcIdkZBOx0bM/H7Nj8qoMxG', 'BE0NKn26hS8sQuxsZ3UY1wD7OsrRROdm', 'pMIOaHgmFsPcfqa2smSOwZiZvFPb3iZ5', NULL);


ALTER TABLE `article`
  ADD PRIMARY KEY (`id`),
  ADD KEY `slug` (`slug`),
  ADD KEY `pos` (`pos`),
  ADD KEY `public` (`public`);

ALTER TABLE `article_tag`
  ADD PRIMARY KEY (`article_id`,`tag_id`),
  ADD KEY `tag_id` (`tag_id`),
  ADD KEY `article_id` (`article_id`);

ALTER TABLE `auth_assignment`
  ADD PRIMARY KEY (`item_name`,`user_id`),
  ADD KEY `user_id` (`user_id`);

ALTER TABLE `auth_item`
  ADD PRIMARY KEY (`name`),
  ADD KEY `rule_name` (`rule_name`),
  ADD KEY `type` (`type`);

ALTER TABLE `auth_item_child`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `child` (`child`);

ALTER TABLE `auth_rule`
  ADD PRIMARY KEY (`name`);

ALTER TABLE `page`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `tag`
  ADD PRIMARY KEY (`id`),
  ADD KEY `slug` (`slug`);

ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role` (`role`),
  ADD KEY `email` (`email`),
  ADD KEY `password` (`password`),
  ADD KEY `auth_key` (`auth_key`),
  ADD KEY `forgot_key` (`forgot_key`);


ALTER TABLE `article`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
ALTER TABLE `page`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
ALTER TABLE `tag`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

ALTER TABLE `article_tag`
  ADD CONSTRAINT `article_tag_ibfk_1` FOREIGN KEY (`article_id`) REFERENCES `article` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `article_tag_ibfk_2` FOREIGN KEY (`tag_id`) REFERENCES `tag` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

ALTER TABLE `auth_assignment`
  ADD CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_assignment_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `auth_item`
  ADD CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

ALTER TABLE `auth_item_child`
  ADD CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
