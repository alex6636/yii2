<?php
/** @var yii\base\View $this */
/** @var string $url */
?>
<?php $this->beginBlock('content'); ?>
    <?=Yii::t('app', 'Click <a href="{url}">here</a> to reset your password.', ['url'=>$url])?>
<?php $this->endBlock(); ?>
