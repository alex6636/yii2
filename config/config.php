<?php
$config = [
    'id'=>'project_name',
    'name'=>'Project Name',
    /*
    'language'=>'en',
    'sourceLanguage'=>'en',
    */
    'language'=>'ru-RU',
    'sourceLanguage'=>'ru',
    'basePath'=>dirname(__DIR__),
    'bootstrap'=>['log'],
    'components'=>require 'components.php',
    'aliases'=>require 'aliases.php',
    'modules'=>[
        'admin'=>[
            'class'=>'app\modules\admin\AdminModule',
        ]
    ],
    'params'=>require 'params.php',
    'defaultRoute'=>'index/index',
];
/*
if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = 'yii\debug\Module';
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = 'yii\gii\Module';
}
*/
return $config;
