<?php
return [
    'request'=>[
        // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
        'cookieValidationKey'=>'tr1gg3r',
        'enableCsrfValidation'=>true,
    ],
    'cache'=>[
        'class'=>'yii\caching\FileCache',
    ],
    'authManager'=>[
        'class'=>'yii\rbac\DbManager'
    ],
    'user'=>[
        'identityClass'=>'app\models\User',
        'enableAutoLogin'=>true,
        'loginUrl' =>'/', // may be changed to the login form of the site
        'returnUrl'=>'/',
    ],
    // if need an independent type of user
    /*
    'moderator'=>[
        'class' => 'yii\web\User',
        'identityClass'=> 'app\models\Moderator',
        'enableAutoLogin'=>true,
        'loginUrl'=>'/moderator/login',
        'returnUrl'=>'/moderator',
        'identityCookie'=>['name'=>'_moderator', 'httpOnly'=>true],
        'idParam' => '__moderator_id',
        'authTimeoutParam'=>'__moderator_expire',
    ],
    */
    'httpcookie'=>[
        'class'=>'yii\web\cookie',
        'httponly'=>true,
    ],
    'errorHandler'=>[
        'errorAction'=>'front/error',
    ],
    'mailer'=>[
        'class'=>'yii\swiftmailer\Mailer',
        // send all mails to a file by default. You have to set
        // 'useFileTransport' to false and configure a transport
        // for the mailer to send real emails.
        'useFileTransport'=>true,
    ],
    'log'=>[
        'traceLevel'=>YII_DEBUG ? 3 : 0,
        'targets'=>[
            /*
            [
                'class'=>'yii\log\FileTarget',
                'levels'=>['error', 'warning'],
            ],
            */
			/*
			[
				'class' => 'yii\log\EmailTarget',
				'levels'=>['error', 'warning'],
				'except'=>['yii\web\HttpException:404'],
				'message' => [
				   'from' => ['noreply@project.com'],
				   'to' => ['support@@project.com'],
				   'subject' => 'Error/warning on project.com',
				],
			],
			*/
        ],
    ],
    'db'=>require __DIR__ . '/db.php',
    'urlManager'=>require __DIR__ . '/url_manager.php',
];
