<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   02.09.2017
 * 
 * Example access filter
 * How to use:
 * 
 * class ExampleController extends Controller
 * {
 *     public function behaviors() {
 *         return [
 *             AccessFilter::class,
 *             // ...
 *         ];
 *     }
 * }
 */

namespace app\filters;
use yii\base\ActionFilter;
use yii\web\BadRequestHttpException;

class AccessFilter extends ActionFilter
{
    public function beforeAction($action) {
        $can_access = false;
        if (!$can_access) {
            throw new BadRequestHttpException;
        }
        return true;
    }
}
