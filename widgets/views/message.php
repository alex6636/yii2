<?php
use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var string $message */
?>
<h1><?=Html::encode($message)?></h1>
