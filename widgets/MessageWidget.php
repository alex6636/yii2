<?php
namespace app\widgets;
use yii\base\Widget;

class MessageWidget extends Widget
{
    public $message;
    
    public function run() {
        return $this->render('message', [
            'message'=>$this->message,
        ]);
    }
} 
