<?php
namespace app\modules\admin\models;
use app\models\User;
use yii\base\Model;

class LoginModel extends Model
{
    public $email;
    public $password;
    public $remember_me;

    /** @var User $User */
    protected $User = NULL;

    public function rules() {
        return [
            [['email', 'password'], 'filter', 'filter'=>'trim'],
            [['email', 'password'], 'required'],
            ['email', 'email'],
            ['remember_me', 'boolean'],
            ['password', 'validatePassword'],
        ];
    }

    public function attributeLabels() {
        return [
            'password'=>\Yii::t('app', 'Password'),
            'remember_me'=>\Yii::t('app', 'Remember me'),
        ];
    }

    public function validatePassword($attribute) {
        if (!$this->hasErrors()) {
            /** @var User $User */
            $User = $this->getUser();
            if (!$User || !$User->validatePassword($this->password)) {
                $this->addError($attribute, \Yii::t('app', 'Incorrect email or password'));
            }
        }
    }

    public function login() {
        if ($this->validate()) {
            /** @var User $User */
            $User = $this->getUser();
            $duration = $this->remember_me ? 3600 * 24 * 30 : 3600;
            if ($User->login($duration)) {
                return true;
            }
            $this->addError('password', \Yii::t('app', 'Incorrect email or password'));
        }
        return false;
    }

    public function getUser() {
        if (!$this->User) {
            $this->User = User::findOne(['email'=>$this->email]);
        }
        return $this->User;
    }
}
