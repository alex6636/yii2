<?php
namespace app\modules\admin\models;
use app\models\User;
use yii\base\Model;
use yii\helpers\Url;
use RuntimeException;

class ForgotModel extends Model
{
    const SCENARIO_FORGOT    = 'forgot';
    const SCENARIO_RESETPASS = 'resetpass';

    public $email;
    public $password;
    public $repeat_password;

    public function rules() {
        return [
            [['email', 'password', 'repeat_password'], 'filter', 'filter'=>'trim'],
            [['email', 'password', 'repeat_password'], 'required'],
            ['email', 'email'],
            ['repeat_password', 'compare', 'compareAttribute'=>'password', 'message'=>\Yii::t('app', 'Passwords don\'t match')],
        ];
    }
    
    public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_FORGOT]    = ['email'];
        $scenarios[self::SCENARIO_RESETPASS] = ['password', 'repeat_password'];
        return $scenarios;
    }

    public function attributeLabels() {
        return [
            'password'=>\Yii::t('app', 'Password'),
            'repeat_password'=>\Yii::t('app', 'Repeat Password'),
        ];
    }

    public function forgot($url_to_reset) {
        if ($this->validate()) {
            /** @var User $User */
            if ($User = User::findOne(['email'=>$this->email])) {
                $forgot_key = \Yii::$app->getSecurity()->generateRandomString();
                $User->updateForgotKey($forgot_key);
                $url = Url::to([$url_to_reset, 'forgot_key'=>$forgot_key], true);
                $this->sendForgotEmail($url);
                return true;
            } else {
                $this->addError('email', \Yii::t('app', 'Email address is invalid'));
            }
        }
        return false;
    }
    
    public function reset($forgot_key) {
        if ($this->validate()) {
            /** @var User $User */
            if ($User = User::findOne(['forgot_key' => $forgot_key])) {
                $User->updatePassword($this->password);
                $User->updateForgotKey(null);
                return true;
            } else {
                $this->addError('password', \Yii::t('app', 'Key is invalid'));
            }
        }
        return false;
    }
    
    protected function sendForgotEmail($url) {
        $from = 'noreply@' . \Yii::$app->request->serverName;
        $to = $this->email;
        $subject = \Yii::t('app', 'Reset password');
        $sent = \Yii::$app->mailer->compose(['html'=>'admin/forgot/html'], ['url'=>$url])
                                 ->setFrom($from)
                                 ->setTo($to)
                                 ->setSubject($subject)
                                 ->send();
        if (!$sent) {
            throw new RuntimeException('Failed to send an email');
        }
    }
}
