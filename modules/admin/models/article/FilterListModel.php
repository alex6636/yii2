<?php
namespace app\modules\admin\models\article;
use app\modules\admin\models\AbstractFilterListModel;
use yii\db\ActiveQuery;

class FilterListModel extends AbstractFilterListModel
{
    public $title;
    public $public;

    /**
     * @param ActiveQuery $ActiveQuery
     */
    public function filter(ActiveQuery $ActiveQuery) {
        if ($this->title) {
            $ActiveQuery->andWhere('title LIKE :title', ['title'=>'%' . $this->title . '%']);
        }
        if (in_array($this->public, ['0', '1'], true)) {
            $ActiveQuery->andWhere(['public'=>$this->public]);
        }
    }

    public function rules() {
        return [
            ['title', 'filter', 'filter'=>'trim'],
            ['public', 'boolean'],
        ];
    }
    
    public function attributeLabels() {
        return [
            'title'=>\Yii::t('app', 'Title'),
        ];
    }
}
