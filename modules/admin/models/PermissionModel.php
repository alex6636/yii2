<?php
namespace app\modules\admin\models;
use yii\base\Model;

class PermissionModel extends Model
{
    public $name;

    public function rules() {
        return [
            ['name', 'filter', 'filter'=>'trim'],
            ['name', 'required'],
            ['name', function($attribute) {
                if (
                    \Yii::$app->authManager->getPermission($this->name) ||
                    \Yii::$app->authManager->getRole($this->name)
                ){
                    $this->addError($attribute, \Yii::t('app', 'The role or permission with the name {name} is already exists', ['name'=>($this->name)]));
                }
            }],
        ];
    }

    public function attributeLabels() {
        return [
            'name'=>\Yii::t('app', 'Permission'),
        ];
    }

    public function createPermission() {
        if ($Permission = \Yii::$app->authManager->createPermission($this->name)) {
            \Yii::$app->authManager->add($Permission);
        }
    }
}
