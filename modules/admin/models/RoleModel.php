<?php
namespace app\modules\admin\models;
use yii\base\InvalidCallException;
use yii\base\Model;
use yii\helpers\ArrayHelper;

class RoleModel extends Model
{
    public $name;
    public $permissions;
    /** @var \yii\rbac\Role|null $Role */
    public $Role = null;

    public function rules() {
        return [
            ['name', 'filter', 'filter'=>'trim'],
            ['name', 'required'],
            ['name', function($attribute) {
                if (
                    (($Role = \Yii::$app->authManager->getRole($this->name)) && ($Role != $this->Role)) ||
                    \Yii::$app->authManager->getPermission($this->name)
                ){
                    $this->addError($attribute, \Yii::t('app', 'The role or permission with the name {name} is already exists', ['name'=>($this->name)]));
                }
            }],
            ['permissions', 'each', 'rule'=>['string']],
        ];
    }

    public function attributeLabels() {
        return [
            'name'=>\Yii::t('app', 'Role'),
            'permissions'=>\Yii::t('app', 'Permissions'),
        ];
    }

    public function createRole() {
        if ($Role = \Yii::$app->authManager->createRole($this->name)) {
            \Yii::$app->authManager->add($Role);
            // save permissions
            if (!empty($this->permissions)) {
                foreach ($this->permissions as $permission_name) {
                    if ($Permission = \Yii::$app->authManager->getPermission($permission_name)) {
                        \Yii::$app->authManager->addChild($Role, $Permission);
                    }
                }
            }
        }
    }

    public function updateRole() {
        if (!$this->Role instanceof \yii\rbac\Role) {
            throw new InvalidCallException('The role is not initialized');
        }
        $old_permissions = \Yii::$app->authManager->getPermissionsByRole($this->Role->name);
        $old_name = $this->Role->name;
        $this->Role->name = $this->name;
        \Yii::$app->authManager->update($old_name, $this->Role);
        if (!empty($old_permissions)) {
            foreach ($old_permissions as $Permission) {
                \Yii::$app->authManager->removeChild($this->Role, $Permission);
            }
        }
        // save permissions
        if (!empty($this->permissions)) {
            foreach ($this->permissions as $permission_name) {
                if ($Permission = \Yii::$app->authManager->getPermission($permission_name)) {
                    \Yii::$app->authManager->addChild($this->Role, $Permission);
                }
            }
        }
    }

    /**
     * @param string $name
     * @return array
     */
    public function getRolePermissions($name) {
        return ArrayHelper::map(\Yii::$app->authManager->getPermissionsByRole($name), 'name', 'name');
    }

    /**
     * @return array
     */
    public function getAllPermissions() {
        return ArrayHelper::map(\Yii::$app->authManager->getPermissions(), 'name', 'name');
    }
}
