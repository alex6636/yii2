<?php
namespace app\modules\admin;
use yii\base\Module;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use DomainException;

class AdminModule extends Module
{
    public function init() {
        parent::init();
        \Yii::configure($this, require(__DIR__ . '/config/main.php'));
    }

    public function getRules() {
        $static_rules = [
            [
                'allow'=>true,
                'controllers'=>['admin/index'],
                'actions'=>['login', 'forgot', 'reset-password'],
                'roles' => ['?'],
            ],
            [
                'allow'=>false,
                'controllers'=>['admin/index'],
                'actions'=>['login', 'forgot', 'reset-password'],
                'roles' => ['@'],
            ],
            [
                'allow'=>true,
                'controllers'=>['admin/index'],
                'roles' => ['@'],
            ],
            // allow to all
            [
                'allow'=>true,
                'roles'=>['administrator'],
            ],
        ];
        $dynamic_rules = [];
        $roles = \Yii::$app->authManager->getRoles();
        foreach ($roles as $Role) {
            $permissions = \Yii::$app->authManager->getPermissionsByRole($Role->name);
            foreach ($permissions as $permission) {
                if (!isset($this->params['access']['rules'][$permission->name])) {
                    throw new DomainException('You must configure access rules to permission "' . $permission->name . '" in the ' .
                                              \Yii::getAlias('@admin') . '/config/params.php');
                }
                $dynamic_rules[] = array_merge($this->params['access']['rules'][$permission->name], [
                    'roles'=>[$Role->name],
                ]);
            }
        }
        $rules = array_merge($static_rules, $dynamic_rules);
        return $rules;
    }

    public function behaviors() {
        return [
            'access'=>[
                'class'=>AccessControl::class,
                'rules'=>$this->getRules(),
                'denyCallback'=>function() {
                    /** @var \app\models\User $identity */
                    if ($identity = \Yii::$app->user->getIdentity()) {
                        /*
                         * for example, you can check the role
                         * if (in_array($identity->role, ['administrator', 'editor'])) { // do something }
                         */
                        throw new ForbiddenHttpException;
                    }
                    \Yii::$app->session->set('admin_redirect_url', \Yii::$app->request->url);
                    return \Yii::$app->response->redirect(Url::to(['index/login']));
                },
            ],
        ];
    }
}
