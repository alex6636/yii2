<?php
return [
    'layout'=>'main',
    'defaultRoute'=>'index/index',
    'aliases'=>[
        '@admin'        =>'@app/modules/admin',
        '@admin_views'  =>'@app/modules/admin/views',
        '@admin_layouts'=>'@app/modules/admin/views/layouts'
    ],
    'params'=>require(__DIR__ . '/params.php'),
];