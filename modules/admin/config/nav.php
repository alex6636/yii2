<?php
use yii\helpers\Url;

$nav = [];
if (Yii::$app->user->can('pages')) {
    $nav[Yii::t('app','Pages')] = [
        'fa_icon' => 'fa-file-alt',
        'items' => [
            Yii::t('app','Home Page')=> [
                'link' => Url::to(['/admin/page/edit', 'id'=>1]),
            ],
            Yii::t('app','Contact')=>[
                'link' => Url::to(['/admin/page/edit', 'id'=>2])
            ],
        ],
    ];
    $nav[Yii::t('app','Gallery')] = [
        'fa_icon' => 'fa-images',
        'link' => Url::to('/admin/gallery'),
    ];
}
if (Yii::$app->user->can('blog')) {
    $nav[Yii::t('app','Blog')] = [
        'fa_icon' => 'fa-rss',
        'items' => [
            Yii::t('app','Articles')=> [
                'link' => Url::to(['/admin/article']),
            ],
            Yii::t('app','Tags')=>[
                'link' => Url::to(['/admin/tag']),
            ],
            Yii::t('app','Article Categories')=> [
                'link' => Url::to(['/admin/article-category']),
                'active'=>'article_category',
            ],
        ],
    ];
}
if (Yii::$app->user->can('admins')) {
    $nav[Yii::t('app','Users')] = [
        'fa_icon'=>'fa-user-plus',
        'items'=>[
            Yii::t('app','Users')=>[
                'link'=>Url::to(['/admin/user']),
            ],
            Yii::t('app','Roles')=>[
                'link'=>Url::to(['/admin/role']),
            ],
            Yii::t('app','Permissions')=>[
                'link'=>Url::to(['/admin/permission']),
            ],
        ],
    ];
}
return $nav;
