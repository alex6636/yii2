<?php
return [
    'access'=>[
        'rules'=>[
            'admins'=>[
                'allow'=>true,
                'controllers'=>[
                    'admin/user',
                    'admin/role',
                    'admin/permission',
                ],
            ],
            'blog'=>[
                'allow'=>true,
                'controllers'=>[
                    'admin/article',
                    'admin/tag',
                ],
            ],
            'pages'=>[
                'allow'=>true,
                'controllers'=>[
                    'admin/page',
                ],
            ],
        ],
    ],
];
