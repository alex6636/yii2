<?php
namespace app\modules\admin\assets;
use yii\web\AssetBundle;

class AdminAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        //'resources/admin/font-awesome-4.7.0/css/font-awesome.min.css',
        'resources/admin/fontawesome-free-5.8.2-web/css/all.min.css',
        'resources/admin/css/sb-admin-2.css',
    ];
    public $js = [
        'resources/admin/js/metisMenu.min.js',
        'resources/admin/js/sb-admin-2.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset'
    ];
} 
