<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   07-Sep-17
 */

namespace app\modules\admin\actions\user;
use app\models\User;
use app\modules\admin\controllers\CrudController;
use alexs\yii2crud\models\CrudModel;
use yii\web\Response;

class EditAction extends \app\modules\admin\actions\base\EditAction
{
    public $scenario = User::SCENARIO_EDIT;

    /**
     * @param CrudModel $Model
     * @return Response
     */
    protected function afterEdit(CrudModel $Model) {
        /** @var CrudController $controller */
        $controller = $this->controller;
        /** @var User $Model */
        $Role = \Yii::$app->authManager->getRole($Model->role);
        \Yii::$app->authManager->revokeAll($Model->id);
        \Yii::$app->authManager->assign($Role, $Model->id);
        $this->displaySuccessMessage($Model);
        return $controller->redirect($controller->getRedirectUrl('@action_index'));
    }
}
