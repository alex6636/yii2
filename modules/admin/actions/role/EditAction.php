<?php
namespace app\modules\admin\actions\role;
use alexs\yii2crud\actions\traits\TraitMessageable;
use app\modules\admin\controllers\RoleController;
use app\modules\admin\models\RoleModel;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

class EditAction extends \yii\base\Action
{
    use TraitMessageable;
    
    /**
     * @param string $name
     * @throws NotFoundHttpException
     * @return string
     */
    public function run($name) {
        if (!$Role = \Yii::$app->authManager->getRole($name)) {
            throw new NotFoundHttpException;
        }
        /** @var RoleController $controller */
        $controller = $this->controller;
        $RoleModel = new RoleModel;
        $RoleModel->name = $name;
        $RoleModel->permissions = $RoleModel->getRolePermissions($name);
        $RoleModel->Role = $Role;
        if ($RoleModel->load(\Yii::$app->request->post()) && $RoleModel->validate()) {
            $RoleModel->updateRole();
            $this->displayMessage(\Yii::t('app', 'Role has been successfully updated'));
            return $controller->redirect(Url::to([\Yii::getAlias('@action_index')]));
        }
        $controller->layout = 'form';
        return $controller->render('form', [
            'RoleModel'=>$RoleModel,
            'name'=>$name,
        ]);
    }
}
