<?php
namespace app\modules\admin\actions\role;
use app\modules\admin\controllers\RoleController;

class IndexAction extends \yii\base\Action
{
    /**
     * @return string
     */
    public function run() {
        /** @var RoleController $controller */
        $controller = $this->controller;
        $controller->layout = 'index';
        return $controller->render('index', [
            'roles'=>\Yii::$app->authManager->getRoles(),
        ]);
    }
}
