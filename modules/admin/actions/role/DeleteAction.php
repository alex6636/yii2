<?php
namespace app\modules\admin\actions\role;
use alexs\yii2crud\actions\traits\TraitMessageable;
use app\modules\admin\controllers\RoleController;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

class DeleteAction extends \yii\base\Action
{
    use TraitMessageable;
    
    /**
     * @param string $name
     * @throws NotFoundHttpException
     * @return string
     */
    public function run($name) {
        if (!$Role = \Yii::$app->authManager->getRole($name)) {
            throw new NotFoundHttpException;
        }
        /** @var RoleController $controller */
        $controller = $this->controller;
        \Yii::$app->authManager->remove($Role);
        $this->displayMessage(\Yii::t('app', 'Role has been successfully deleted'));
        return $controller->redirect(Url::to([\Yii::getAlias('@action_index')]));
    }
}
