<?php
namespace app\modules\admin\actions\role;
use alexs\yii2crud\actions\traits\TraitMessageable;
use app\modules\admin\controllers\RoleController;
use app\modules\admin\models\RoleModel;
use yii\helpers\Url;

class AddAction extends \yii\base\Action
{
    use TraitMessageable;
    
    /**
     * @return string
     */
    public function run() {
        /** @var RoleController $controller */
        $controller = $this->controller;
        $RoleModel = new RoleModel;
        if ($RoleModel->load(\Yii::$app->request->post()) && $RoleModel->validate()) {
            $RoleModel->createRole();
            $this->displayMessage(\Yii::t('app', 'Role has been successfully added'));
            return $controller->redirect(Url::to([\Yii::getAlias('@action_index')]));
        }
        $controller->layout = 'form';
        return $controller->render('form', [
            'RoleModel'=>$RoleModel,
        ]);
    }
    
    
}
