<?php
namespace app\modules\admin\actions\permission;
use alexs\yii2crud\actions\traits\TraitMessageable;
use app\modules\admin\controllers\PermissionController;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

class DeleteAction extends \yii\base\Action
{
    use TraitMessageable;
    
    /**
     * @param string $name
     * @throws NotFoundHttpException
     * @return string
     */
    public function run($name) {
        if (!$permission = \Yii::$app->authManager->getPermission($name)) {
            throw new NotFoundHttpException;
        }
        /** @var PermissionController $controller */
        $controller = $this->controller;
        \Yii::$app->authManager->remove($permission);
        $this->displayMessage(\Yii::t('app', 'Permission has been successfully deleted'));
        return $controller->redirect(Url::to([\Yii::getAlias('@action_index')]));
    }
}
