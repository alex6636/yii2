<?php
namespace app\modules\admin\actions\permission;
use app\modules\admin\controllers\PermissionController;

class IndexAction extends \yii\base\Action
{
    /**
     * @return string
     */
    public function run() {
        /** @var PermissionController $controller */
        $controller = $this->controller;
        $controller->layout = 'index';
        return $controller->render('index', [
            'permissions'=>\Yii::$app->authManager->getPermissions(),
        ]);
    }
}
