<?php
namespace app\modules\admin\actions\permission;
use alexs\yii2crud\actions\traits\TraitMessageable;
use app\modules\admin\controllers\PermissionController;
use app\modules\admin\models\PermissionModel;
use yii\helpers\Url;

class AddAction extends \yii\base\Action
{
    use TraitMessageable;
    
    /**
     * @return string
     */
    public function run() {
        /** @var PermissionController $controller */
        $controller = $this->controller;
        $PermissionModel = new PermissionModel;
        if ($PermissionModel->load(\Yii::$app->request->post()) && $PermissionModel->validate()) {
            $PermissionModel->createPermission();
            $this->displayMessage(\Yii::t('app', 'Permission has been successfully added'));
            return $controller->redirect(Url::to([\Yii::getAlias('@action_index')]));
        }
        $controller->layout = 'form';
        return $controller->render('form', [
            'PermissionModel'=>$PermissionModel,
        ]);
    }
}
