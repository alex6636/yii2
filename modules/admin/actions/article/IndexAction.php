<?php
namespace app\modules\admin\actions\article;
use alexs\yii2lists\actions\traits\TraitFilterUpdateListAction;
use app\models\Article;
use app\modules\admin\models\article\FilterListModel;
use yii\db\ActiveQuery;

class IndexAction extends \app\modules\admin\actions\base\IndexAction
{
    use TraitFilterUpdateListAction;

    /**
     * @return ActiveQuery
     */
    public function findItems() {
        return Article::findItems();
    }

    /**
     * @return FilterListModel
     */
    public function getFilterListModel() {
        return new FilterListModel;
    }
}
