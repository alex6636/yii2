<?php
use app\models\ArticleCategory;
use app\modules\admin\controllers\CrudController;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\web\View;

/** @var View $this */
/** @var ArticleCategory $Model */
/** @var CrudController $controller */

$controller = Yii::$app->controller;
$action = $Model->id ? $controller->getRedirectUrl('@action_edit', ['id'=>$Model->id]) : $controller->getRedirectUrl('@action_add');
$this->title = Yii::t('app',($Model->id ? 'Edit' : 'Add') . ' Category');
$this->params['buttons'] = [
    Yii::t('app','Back to the list')=> [
        'icon'=>'fa-arrow-circle-left',
        'link'=>$controller->getRedirectUrl('@action_index'),
    ],
];
$this->params['breadcrumbs'] = [
    Yii::t('app', 'Home')=>Url::to('/admin'),
    Yii::t('app','Article Categories')=>$controller->getRedirectUrl('@action_index'),
    $this->title=>null,
];
$this->params['active'] = 'article_category';
?>
<?php $this->beginBlock('form'); ?>
    <?php $ActiveForm = ActiveForm::begin([
        'action' =>$action,
    ]); ?>
    <fieldset>
        <?=$ActiveForm->field($Model, 'name');?>
        <?=Html::submitButton(Yii::t('app', 'Save'), ['class'=>'btn btn-lg btn-warning', 'name'=>'btn-save-form']);?>
    </fieldset>
    <?php ActiveForm::end(); ?>
<?php $this->endBlock(); ?>
