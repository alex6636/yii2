<?php
use app\models\Page;
use app\modules\admin\controllers\CrudController;
use app\modules\admin\widgets\ActiveFormTextEditor;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\web\View;

/** @var View $this */
/** @var Page $Model */
/** @var CrudController $controller */

$controller = Yii::$app->controller;
$action = $controller->getRedirectUrl('@action_edit', ['id'=>$Model->id]);
$this->title = Yii::t('app','Edit ' . $Model::getHumanModelName());
$this->params['breadcrumbs'] = [
    Yii::t('app', 'Home')=>Url::to('/admin'),
    Yii::t('app',$Model::getHumanModelName() . 's')=>null,
    $this->title=>null,
];
?>
<?php $this->beginBlock('form'); ?>
    <?php $ActiveForm = ActiveForm::begin([
        'action' =>$action,
    ]); ?>
    <fieldset>
        <?=$ActiveForm->field($Model, 'title');?>
        <?=ActiveFormTextEditor::widget(['ActiveForm'=>$ActiveForm, 'Model'=>$Model]);?>
        <?=Html::submitButton(Yii::t('app','Save'), ['class'=>'btn btn-lg btn-warning', 'name'=>'btn-save-form']);?>
    </fieldset>
    <?php ActiveForm::end(); ?>
<?php $this->endBlock(); ?>
