<?php
use app\modules\admin\models\LoginModel;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var yii\bootstrap\ActiveForm $ActiveForm */
/** @var LoginModel $LoginModel */
?>
<?php $this->beginBlock('content'); ?>
    <div class="login-panel panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"><?=Html::encode(Yii::$app->name);?> - <?=Yii::t('app', 'Admin Panel')?></h3>
        </div>
        <div class="panel-body">
            <?php $ActiveForm = ActiveForm::begin([
                'fieldConfig'=>[
                    'template'=>"{input}\n{error}",
                ],
            ]); ?>
            <fieldset>
                <?=$ActiveForm->field($LoginModel, 'email', ['inputOptions'=>['placeholder'=>$LoginModel->getAttributeLabel('email'), 'autofocus'=>'on']]); ?>
                <?=$ActiveForm->field($LoginModel, 'password', ['inputOptions'=>['placeholder'=>$LoginModel->getAttributeLabel('password')]])->passwordInput(); ?>
                <?=$ActiveForm->field($LoginModel, 'remember_me', [
                    'checkboxTemplate'=>'<div class="checkbox"><label>{input}' . $LoginModel->getAttributeLabel('remember_me') . '</label><a class="pull-right" href="' . Url::to('forgot') . '">' . Yii::t('app', 'Forgot password') . '</a></div>',
                ])->checkbox() ?>
                <?=Html::submitButton(Yii::t('app', 'Sign in'), ['class'=>'btn btn-lg btn-info btn-block']) ?>
            </fieldset>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
<?php $this->endBlock(); ?>
