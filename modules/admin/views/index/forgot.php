<?php
use app\modules\admin\models\ForgotModel;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var yii\bootstrap\ActiveForm $ActiveForm */
/** @var ForgotModel $ForgotModel */
?>
<?php $this->beginBlock('content'); ?>
    <div class="login-panel panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"><?=Html::encode(Yii::$app->name);?> - <?=Yii::t('app', 'Admin Panel')?></h3>
        </div>
        <div class="panel-body">
            <?=$this->render('/layouts/blocks/message');?>
            <?php $ActiveForm = ActiveForm::begin([
                'fieldConfig'=>['template'=>"{input}\n{error}"],
            ]); ?>
            <fieldset>
                <?=$ActiveForm->field($ForgotModel, 'email', ['inputOptions'=>['placeholder'=>$ForgotModel->getAttributeLabel('email'), 'autofocus'=>'on']]); ?>
                <div class="form-group">
                    <?=Html::submitButton(Yii::t('app', 'Submit'), ['class'=>'btn btn-lg btn-info']) ?>
                    <a class="btn" href="<?=Url::to('login')?>"><?=Yii::t('app', 'Sign in')?></a>
                </div>
            </fieldset>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
<?php $this->endBlock(); ?>
