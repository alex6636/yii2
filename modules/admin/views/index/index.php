<?php
use app\models\User;
use yii\helpers\Url;
use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var User $User */
?>
<?php $this->beginBlock('content'); ?>
    <div class="panel panel-default">
        <div class="panel-heading">
            <?=Yii::t('app', 'You are logged in as')?>
            <?php if (Yii::$app->user->can('admins')) { ?>
                <strong><a href="<?=Url::to(['user/edit', 'id'=>Yii::$app->user->getId()]);?>"><?=Html::encode($User->name);?></a></strong>
            <?php } else { ?>
                <strong><?=Html::encode($User->name);?></strong>
            <?php } ?>
        </div>
    </div>
<?php $this->endBlock(); ?>
