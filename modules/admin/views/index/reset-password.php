<?php
use app\modules\admin\models\ForgotModel;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var yii\bootstrap\ActiveForm $ActiveForm */
/** @var ForgotModel $ForgotModel */
?>
<?php $this->beginBlock('content'); ?>
    <div class="login-panel panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"><?=Html::encode(Yii::$app->name);?> - <?=Yii::t('app', 'Admin Panel')?></h3>
        </div>
        <div class="panel-body">
            <?=$this->render('/layouts/blocks/message');?>
            <?php $ActiveForm = ActiveForm::begin([
                'fieldConfig'=>[
                    'template'=>"{input}\n{error}",
                ],
            ]); ?>
            <fieldset>
                <?=$ActiveForm->field($ForgotModel, 'password', ['inputOptions'=>['placeholder'=>$ForgotModel->getAttributeLabel('password'), 'autofocus'=>'on', 'autocomplete'=>'off']])->passwordInput(); ?>
                <?=$ActiveForm->field($ForgotModel, 'repeat_password', ['inputOptions'=>['placeholder'=>$ForgotModel->getAttributeLabel('repeat_password'), 'autocomplete'=>'off']])->passwordInput(); ?>
                <?=Html::submitButton(Yii::t('app', 'Submit'), ['class'=>'btn btn-lg btn-info']) ?>
                <a class="btn" href="<?=Url::to('login')?>"><?=Yii::t('app', 'Sign in')?></a>
            </fieldset>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
<?php $this->endBlock(); ?>
