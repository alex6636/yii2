<?php
use yii\rbac\Role;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var Role[] $roles */

$this->title = Yii::t('app', 'Roles');
$this->params['buttons'] = [
    Yii::t('app', 'Add')=> [
        'icon'=>'fa-plus-square',
        'link'=>Url::to([Yii::getAlias('@action_add')])
    ],
];
$this->params['breadcrumbs'] = [
    Yii::t('app','Home')=>Url::to(['/admin']),
    $this->title=>null,
];
?>
<?php $this->beginBlock('list'); ?>
    <?php if (!empty($roles)) { ?>
        <table class="table table-striped table-bordered table-hover">
            <tr>
                <th width="100%"><?=Yii::t('app','Role')?></th>
                <th><?=Yii::t('app','Actions')?></th>
            </tr>
            <?php foreach ($roles as $Role) { ?>
                <tr class="odd gradeX">
                    <td><?=Html::encode($Role->name);?></td>
                    <td class="action-column">
                        <a href="<?=Url::to([Yii::getAlias('@action_edit'), 'name'=>$Role->name])?>" class="btn btn-xs btn-warning" title="<?=Yii::t('app','Edit')?>">
                            <i class="fa fa-edit"></i> <?=Yii::t('app','Edit')?>
                        </a>
                        <a href="<?=Url::to([Yii::getAlias('@action_delete'), 'name'=>$Role->name])?>" class="btn btn-xs btn-danger"
                            onclick="return confirm('<?=Yii::t('app','Are you sure?')?>')" title="<?=Yii::t('app','Delete')?>">
                            <i class="fa fa-trash-alt"></i> <?=Yii::t('app','Delete')?>
                        </a>
                    </td>
                </tr>
            <?php } ?>
        </table>
    <?php } else { ?>
        <?=$this->render('/layouts/blocks/list-empty-message');?>
    <?php } ?>
<?php $this->endBlock(); ?>
