<?php
use app\modules\admin\models\RoleModel;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\web\View;

/** @var View $this */
/** @var RoleModel $RoleModel */
/** @var string $name */

$action = isset($name) ? Url::to([Yii::getAlias('@action_edit'), 'name'=>$name]) : Url::to([Yii::getAlias('@action_add')]);
$this->title = Yii::t('app', (isset($name) ? 'Edit' : 'Add') . ' Role');
$this->params['buttons'] = [
    Yii::t('app', 'Back to the list')=> [
        'icon'=>'fa-arrow-circle-left',
        'link'=>Url::to([Yii::getAlias('@action_index')])
    ],
];
$this->params['breadcrumbs'] = [
    Yii::t('app', 'Home')=>Url::to('/admin'),
    Yii::t('app','Roles')=>Url::to([Yii::getAlias('@action_index')]),
    $this->title=>null,
];
?>
<?php $this->beginBlock('form'); ?>
    <?php $ActiveForm = ActiveForm::begin([
        'action'=>$action,
    ]); ?>
    <fieldset>
        <?=$ActiveForm->field($RoleModel, 'name');?>
        <?=$ActiveForm->field($RoleModel, 'permissions')->checkboxList($RoleModel->getAllPermissions());?>
        <?=Html::submitButton(Yii::t('app','Save'), ['class'=>'btn btn-lg btn-warning', 'name'=>'btn-save-form']);?>
    </fieldset>
    <?php ActiveForm::end(); ?>
<?php $this->endBlock(); ?>
