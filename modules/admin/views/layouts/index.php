<?php
/** @var yii\web\View $this */
?>
<?php require 'interface/header.php'; ?>
<div id="wrapper">
    <?php require 'interface/nav.php'; ?>
    <div id="page-wrapper">
        <?php require 'interface/head.php'; ?>
        <?php require 'interface/breadcrumbs.php'; ?>
        <?php require 'interface/buttons.php'; ?>
        <div class="row">
            <div class="col-lg-12">
                <?=$this->render('blocks/message');?>
                <?=isset($this->blocks['list']) ? $this->blocks['list'] : ''; ?>
            </div>
        </div>
    </div>
</div>
<?php require 'interface/footer.php'; ?>
