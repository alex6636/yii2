<?php
use yii\helpers\Html;

/** @var yii\web\View $this */
?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?=Html::encode($this->title); ?></h1>
    </div>
</div>
