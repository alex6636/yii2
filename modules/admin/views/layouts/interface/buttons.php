<?php
use yii\helpers\Html;

/** @var yii\web\View $this */
?>
<?php if (!empty($this->params['buttons'])) { ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="admin-action-btn btn-group btn-group-justified">
                <?php foreach ($this->params['buttons'] as $name=>$data) { ?>
                    <?php $icon = $data['icon'] ? '<i class="fa ' . $data['icon'] . '"></i>' : ''; ?>
                    <a class="btn btn-info" href="<?=$data['link']?>"><?=$icon?> <?=Html::encode($name);?></a>
                <?php } ?>
            </div>
        </div>
    </div>
<?php } ?>
