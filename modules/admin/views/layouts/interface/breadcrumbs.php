<?php
use yii\helpers\Html;

/** @var yii\web\View $this */
?>
<?php if (!empty($this->params['breadcrumbs'])) { ?>
    <div class="row">
        <div class="col-lg-12">
            <ol class="breadcrumb">
                <?php foreach ($this->params['breadcrumbs'] as $title=>$link) { ?>
                    <?php if ($link) { ?>
                        <li><a href="<?=$link?>"><?=Html::encode($title);?></a></li>
                    <?php } else { ?>
                        <li class="active"><?=Html::encode($title);?></li>
                    <?php } ?>
                <?php } ?>
            </ol>
        </div>
    </div>
<?php } ?>
