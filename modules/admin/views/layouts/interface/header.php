<?php
use app\modules\admin\assets\AdminAsset;
use yii\helpers\Html;

/** @var yii\web\View $this */
AdminAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?=Yii::$app->language ?>">
<head>
    <meta charset="<?=Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta name="author" content="Alex Sergey"/>
    <?=Html::csrfMetaTags()?>
    <title><?=$this->title ? Html::encode($this->title) . ' | ' : '';?><?=Html::encode(Yii::$app->name);?> | <?=Yii::t('app', 'Admin Panel')?></title>
    <?php $this->head() ?>
</head>
<body class="<?=Yii::$app->controller->id?> <?=Yii::$app->controller->action->id?>">
    <?php $this->beginBody() ?>
