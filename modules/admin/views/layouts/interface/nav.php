<?php
use app\modules\admin\widgets\Navigation;
use yii\helpers\Html;
use yii\helpers\Url;
?>
<nav class="navbar navbar-default navbar-static-top" role="navigation">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only"><?=Yii::t('app', 'Navigation')?></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="<?=Url::to(['/admin'])?>"><?=Html::encode(Yii::$app->name); ?></a>
    </div>
    <ul class="nav navbar-top-links navbar-right">
        <li class="pull-right">
            <a href="<?=Url::home(); ?>" target="_blank">
                <?=Yii::t('app', 'Go to site')?>
                <i class="fa fa-arrow-right"></i>
            </a>
        </li>
        <li class="dropdown pull-right">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <i class="fa fa-user fa-fw"></i>
                <?=Yii::t('app', 'Settings')?>
                <i class="fa fa-caret-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-user">
                <?php if (Yii::$app->user->can('admins')) { ?>
                    <li>
                        <a href="<?=Url::to(['user/edit', 'id'=>Yii::$app->user->getId()]);?>">
                            <i class="fa fa-user fa-fw"></i> <?=Yii::t('app', 'Your profile')?>
                        </a>
                    </li>
                    <li class="divider"></li>
                <?php } ?>
                <li><a href="<?=Url::to(['index/logout']) ?>"><i class="fa fa-sign-out fa-fw"></i><?=Yii::t('app', 'Logout')?></a></li>
            </ul>
        </li>
    </ul>
    <?=Navigation::widget(['active'=>isset($this->params['active']) ? $this->params['active'] : Yii::$app->controller->id]); ?>
</nav>
