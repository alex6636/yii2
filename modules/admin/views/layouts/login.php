<?php
/** @var yii\web\View $this */
?>
<?php require 'interface/header.php'; ?>
<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <?=isset($this->blocks['content']) ? $this->blocks['content'] : ''; ?>
        </div>
    </div>
</div>
<?php require 'interface/footer.php'; ?>
