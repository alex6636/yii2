<?php
/** @var yii\web\View $this */
?>
<?php require 'interface/header.php'; ?>
<div id="wrapper">
    <?php require 'interface/nav.php'; ?>
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"><?=Yii::t('app', 'Admin Panel')?></h1>
            </div>
        </div>
        <?php require 'interface/breadcrumbs.php'; ?>
        <?php require 'interface/buttons.php'; ?>
        <?=isset($this->blocks['content']) ? $this->blocks['content'] : ''; ?>
    </div>
</div>
<?php require 'interface/footer.php'; ?>
