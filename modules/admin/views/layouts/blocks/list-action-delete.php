<?php
use app\modules\admin\controllers\CrudController;

/** @var yii\web\View $this */
/** @var yii\db\ActiveRecord $Model */
/** @var CrudController $controller */

$controller = Yii::$app->controller;
?>
<a href="<?=$controller->getRedirectUrl('@action_delete', ['id'=>$Model->id])?>" class="btn btn-xs btn-danger"
   onclick="return confirm('<?=Yii::t('app', 'Are you sure?')?>')" title="<?=Yii::t('app', 'Delete')?>">
    <i class="fa fa-trash-alt"></i> <?=Yii::t('app', 'Delete')?>
</a>
