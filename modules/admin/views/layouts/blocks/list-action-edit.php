<?php
use app\modules\admin\controllers\CrudController;

/** @var yii\web\View $this */
/** @var yii\db\ActiveRecord $Model */
/** @var CrudController $controller */

$controller = Yii::$app->controller;
?>
<a href="<?=$controller->getRedirectUrl('@action_edit', ['id'=>$Model->id])?>" class="btn btn-xs btn-warning" title="<?=Yii::t('app', 'Edit')?>">
    <i class="fa fa-edit"></i> <?=Yii::t('app', 'Edit')?>
</a>
