<?php
use app\modules\admin\controllers\CrudController;
use yii\web\View;

/** @var yii\web\View $this */
/** @var CrudController $controller */

$controller = Yii::$app->controller;
?>
<div class="alert alert-info"><?=Yii::t('app', 'No records found')?>. <a class="alert-link" href="<?=$controller->getRedirectUrl('@action_add')?>"><?=Yii::t('app', 'Click here to add')?></a></div>
