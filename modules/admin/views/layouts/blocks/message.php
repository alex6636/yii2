<?php
/** @var yii\web\View $this */
/** @var array|null $keys */
?>
<?php if (Yii::$app->session->hasFlash('message')) { ?>
    <div class="alert alert-info"><?=Yii::$app->session->getFlash('message');?></div>
<?php } ?>
<?php if (Yii::$app->session->hasFlash('error')) { ?>
    <div class="alert alert-danger"><?=Yii::$app->session->getFlash('error');?></div>
<?php } ?>
