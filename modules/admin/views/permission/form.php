<?php
use app\modules\admin\models\PermissionModel;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\web\View;

/** @var View $this */
/** @var PermissionModel $PermissionModel */

$action = Url::to([Yii::getAlias('@action_add')]);
$this->title = Yii::t('app', 'Add Permission');
$this->params['buttons'] = [
    Yii::t('app','Back to the list')=> [
        'icon'=>'fa-arrow-circle-left',
        'link'=>Url::to([Yii::getAlias('@action_index')])
    ],
];
$this->params['breadcrumbs'] = [
    Yii::t('app', 'Home')=>Url::to('/admin'),
    Yii::t('app','Permissions')=>Url::to([Yii::getAlias('@action_index')]),
    $this->title=>null,
];
?>
<?php $this->beginBlock('form'); ?>
    <?php $ActiveForm = ActiveForm::begin([
        'action'=>$action,
    ]); ?>
    <fieldset>
        <?=$ActiveForm->field($PermissionModel, 'name');?>
        <?=Html::submitButton(Yii::t('app','Save'), ['class'=>'btn btn-lg btn-warning', 'name'=>'btn-save-form']);?>
    </fieldset>
    <?php ActiveForm::end(); ?>
<?php $this->endBlock(); ?>
