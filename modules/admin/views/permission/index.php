<?php
use yii\rbac\Permission;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var Permission[] $permissions */

$this->title = Yii::t('app', 'Permissions');
$this->params['buttons'] = [
    Yii::t('app', 'Add')=> [
        'icon'=>'fa-plus-square',
        'link'=>Url::to([Yii::getAlias('@action_add')])
    ],
];
$this->params['breadcrumbs'] = [
    Yii::t('app', 'Home')=>Url::to(['/admin']),
    $this->title=>null,
];
?>
<?php $this->beginBlock('list'); ?>
    <?php if (!empty($permissions)) { ?>
        <table class="table table-striped table-bordered table-hover">
            <tr>
                <th width="100%"><?=Yii::t('app', 'Permission')?></th>
                <th><?=Yii::t('app', 'Actions')?></th>
            </tr>
            <?php foreach ($permissions as $permission) { ?>
                <tr class="odd gradeX">
                    <td><?=Html::encode($permission->name);?></td>
                    <td class="action-column">
                        <a href="<?=Url::to([Yii::getAlias('@action_delete'), 'name'=>$permission->name])?>" class="btn btn-xs btn-danger"
                            onclick="return confirm('<?=Yii::t('app','Are you sure?')?>')" title="<?=Yii::t('app','Delete')?>">
                            <i class="fa fa-trash-alt"></i> <?=Yii::t('app','Delete')?>
                        </a>
                    </td>
                </tr>
            <?php } ?>
        </table>
    <?php } else { ?>
        <?=$this->render('/layouts/blocks/list-empty-message');?>
    <?php } ?>
<?php $this->endBlock(); ?>
