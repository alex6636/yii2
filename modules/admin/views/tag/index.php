<?php
use app\models\Tag;
use app\modules\admin\controllers\CrudController;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use yii\data\Pagination;

/** @var yii\web\View $this */
/** @var Tag[] $models */
/** @var Pagination $Pagination */
/** @var CrudController $controller */

$controller = Yii::$app->controller;
$this->title = Yii::t('app', $controller->getModelClassName() . 's');
$this->params['buttons'] = [
    Yii::t('app','Add')=> [
        'icon'=>'fa-plus-square',
        'link'=>$controller->getRedirectUrl('@action_add'),
    ],
];
$this->params['breadcrumbs'] = [
    Yii::t('app', 'Home')=>Url::to(['/admin']),
    $this->title=>null,
];
?>
<?php $this->beginBlock('list'); ?>
    <?php if (!empty($models)) { ?>
        <table class="table table-striped table-bordered table-hover">
            <tr>
                <th width="100%"><?=$models[0]->getAttributeLabel('name')?></th>
                <th><?=Yii::t('app', 'Actions')?></th>
            </tr>
            <?php foreach ($models as $Model) { ?>
                <tr class="odd gradeX">
                    <td><?=Html::encode($Model->name);?></td>
                    <td class="action-column">
                        <?=$this->render('/layouts/blocks/list-action-edit', ['Model'=>$Model]);?>
                        <?=$this->render('/layouts/blocks/list-action-delete', ['Model'=>$Model]);?>
                    </td>
                </tr>
            <?php } ?>
        </table>
        <div class="pagination_cont"><?=LinkPager::widget(['pagination'=>$Pagination]);?></div>
    <?php } else { ?>
        <?=$this->render('/layouts/blocks/list-empty-message');?>
    <?php } ?>
<?php $this->endBlock(); ?>
