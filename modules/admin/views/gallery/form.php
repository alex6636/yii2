<?php
use app\models\Gallery;
use app\modules\admin\controllers\CrudController;
use alexs\alexgallery\widgets\AlexGallery;
use yii\web\View;
use yii\helpers\Url;

/** @var View $this */
/** @var CrudController $controller */

$controller = Yii::$app->controller;
$this->title = Yii::t('app','Gallery');
$this->params['breadcrumbs'] = [
    Yii::t('app', 'Home')=>Url::to('/admin'),
    $this->title=>null,
];
?>
<?php $this->beginBlock('form'); ?>
    <fieldset>
        <?=AlexGallery::widget([
            'Model'=>new Gallery,
            'upload_url'=>'/admin/gallery/upload',
            'delete_url'=>'/admin/gallery/delete',
            'sort_url'=>'/admin/gallery/sort'
        ])?>
    </fieldset>
<?php $this->endBlock(); ?>
