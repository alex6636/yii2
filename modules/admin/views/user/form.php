<?php
use app\models\User;
use app\modules\admin\controllers\CrudController;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use yii\web\View;

/** @var View $this */
/** @var User $Model */
/** @var CrudController $controller */

$controller = Yii::$app->controller;
$action = $Model->id ? $controller->getRedirectUrl('@action_edit', ['id'=>$Model->id]) : $controller->getRedirectUrl('@action_add');
$roles_arr = array_map(function($val) {
        return Yii::t('app', $val);
    },
    ArrayHelper::map(
        ArrayHelper::toArray(Yii::$app->authManager->getRoles()), 'name', 'name'
    )
);
$this->title = Yii::t('app', ($Model->id ? 'Edit' : 'Add') . ' ' . $Model::getHumanModelName());
$this->params['buttons'] = [
    Yii::t('app', 'Back to the list')=> [
        'icon'=>'fa-arrow-circle-left',
        'link'=>$controller->getRedirectUrl('@action_index'),
    ]
];
$this->params['breadcrumbs'] = [
    Yii::t('app', 'Home')=>Url::to('/admin'),
    Yii::t('app', $Model::getHumanModelName() . 's')=>$controller->getRedirectUrl('@action_index'),
    $this->title=>null,
];
?>
<?php $this->beginBlock('form'); ?>
    <?php $ActiveForm = ActiveForm::begin([
        'action'=>$action,
    ]); ?>
    <fieldset>
        <?=$ActiveForm->field($Model, 'name');?>
        <?=$ActiveForm->field($Model, 'email');?>
        <?=$ActiveForm->field($Model, 'password', ['inputOptions'=>['autocomplete'=>'new-password']])->passwordInput(['value'=>'']);?>
        <?=$ActiveForm->field($Model, 'role')->dropDownList($roles_arr);?>
        <?=Html::submitButton(Yii::t('app', 'Save'), ['class'=>'btn btn-lg btn-warning', 'name'=>'btn-save-form']) ?>
    </fieldset>
    <?php ActiveForm::end(); ?>
<?php $this->endBlock(); ?>
