<?php
use app\models\Article;
use app\models\ArticleCategory;
use app\modules\admin\controllers\CrudController;
use app\modules\admin\models\article\FilterListModel;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use yii\data\Pagination;

/** @var yii\web\View $this */
/** @var Article[] $models */
/** @var FilterListModel $FilterListModel */
/** @var Pagination $Pagination */
/** @var CrudController $controller */

$controller = Yii::$app->controller;
$this->title = Yii::t('app', $controller->getModelClassName() . 's');
$this->params['buttons'] = [
    Yii::t('app','Add')=> [
        'icon'=>'fa-plus-square',
        'link'=>$controller->getRedirectUrl('@action_add'),
    ],
];
$this->params['breadcrumbs'] = [
    Yii::t('app', 'Home')=>Url::to(['/admin']),
    $this->title=>null,
];
?>
<?php $this->beginBlock('list'); ?>
    <?=$this->render('_filter-list-form', ['FilterListModel'=>$FilterListModel])?>
    <?php if (!empty($models)) { ?>
        <?php $ActiveForm = ActiveForm::begin([
            'action'=>$controller->getRedirectUrl('@action_index', ['page'=>Yii::$app->request->get('page')]),
        ]); ?>
            <table class="table table-striped table-bordered table-hover">
                <tr>
                    <th>#</th>
                    <th width="20%"><?=$models[0]->getAttributeLabel('title')?></th>
                    <th width="40%"><?=$models[0]->getAttributeLabel('short_text')?></th>
                    <th width="20%"><?=$models[0]->getAttributeLabel('article_category_id')?></th>
                    <th><?=$models[0]->getAttributeLabel('public')?></th>
                    <th><?=Yii::t('app', 'Actions')?></th>
                </tr>
                <?php foreach ($models as $i=>$Model) { ?>
                    <tr class="odd gradeX">
                        <td><?=$ActiveForm->field($Model, "[$i]pos", ['options'=>['style'=>'width:50px']])->label(false);?></td>
                        <td><?=$ActiveForm->field($Model, "[$i]title")->label(false);?></td>
                        <td><?=$ActiveForm->field($Model, "[$i]short_text")->textarea()->label(false);?></td>
                        <td><?=$ActiveForm->field($Model, "[$i]article_category_id")->dropDownList([''=>''] + ArticleCategory::listAll('name'))->label(false);?></td>
                        <td><?=$ActiveForm->field($Model, "[$i]public")->checkbox(['label'=>null]);?></td>
                        <td class="action-column">
                            <?=$this->render('/layouts/blocks/list-action-edit', ['Model'=>$Model]);?>
                            <?=$this->render('/layouts/blocks/list-action-delete', ['Model'=>$Model]);?>
                        </td>
                    </tr>
                <?php } ?>
            </table>
            <?=Html::submitButton(Yii::t('app', 'Save'), ['class'=>'btn btn-info', 'name'=>'btn-save-form']);?>
        <?php ActiveForm::end(); ?>
        <div class="pagination_cont"><?=LinkPager::widget(['pagination'=>$Pagination]);?></div>
    <?php } else { ?>
        <?=$this->render('/layouts/blocks/list-empty-message');?>
    <?php } ?>
<?php $this->endBlock(); ?>
