<?php
use alexs\alexgallery\widgets\AlexGallery;
use app\models\Article;
use app\models\ArticleCategory;
use app\models\ArticleGallery;
use app\models\Tag;
use app\modules\admin\widgets\ActiveFormTextEditor;
use app\modules\admin\widgets\ActiveFormImage;
use app\modules\admin\widgets\ActiveFormFile;
use app\modules\admin\controllers\CrudController;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\web\View;

/** @var View $this */
/** @var Article $Model */
/** @var CrudController $controller */

$controller = Yii::$app->controller;
$action = $Model->id ? $controller->getRedirectUrl('@action_edit', ['id'=>$Model->id]) : $controller->getRedirectUrl('@action_add');
$this->title = Yii::t('app',($Model->id ? 'Edit' : 'Add') . ' ' . $Model::getHumanModelName());
$this->params['buttons'] = [
    Yii::t('app','Back to the list')=> [
        'icon'=>'fa-arrow-circle-left',
        'link'=>$controller->getRedirectUrl('@action_index'),
    ],
];
$this->params['breadcrumbs'] = [
    Yii::t('app', 'Home')=>Url::to('/admin'),
    Yii::t('app',$Model::getHumanModelName() . 's')=>$controller->getRedirectUrl('@action_index'),
    $this->title=>null,
];
?>
<?php $this->beginBlock('form'); ?>
    <?php $ActiveForm = ActiveForm::begin([
        'action' =>$action,
        'options'=>['enctype'=>'multipart/form-data'],
    ]); ?>
    <fieldset>
        <div class="row">
            <div class="col-md-6">
                <?=$ActiveForm->field($Model, 'title');?>
                <?=$ActiveForm->field($Model, 'short_text')->textarea();?>
                <?=ActiveFormTextEditor::widget(['ActiveForm'=>$ActiveForm, 'Model'=>$Model]);?>
                <?=$ActiveForm->field($Model, 'tag_id')->checkboxList(Tag::listAll('name'));?>
                <?=$ActiveForm->field($Model, 'public')->checkbox();?>
            </div>
            <div class="col-md-6">
                <?=$ActiveForm->field($Model, 'article_category_id')->dropDownList([''=>''] + ArticleCategory::listAll('name'));?>
                <?=ActiveFormImage::widget(['ActiveForm'=>$ActiveForm, 'Model'=>$Model]);?>
                <?=ActiveFormFile::widget(['ActiveForm'=>$ActiveForm, 'Model'=>$Model]);?>
                <?=AlexGallery::widget([
                    'Model'=>new ArticleGallery,
                    'RelationModel'=>$Model,
                    'upload_url'=>'/admin/article-gallery/upload',
                    'delete_url'=>'/admin/article-gallery/delete',
                    'sort_url'=>'/admin/article-gallery/sort'
                ])?>
            </div>
        </div>
        <?=Html::submitButton(Yii::t('app','Save'), ['class'=>'btn btn-lg btn-warning', 'name'=>'btn-save-form']);?>
    </fieldset>
    <?php ActiveForm::end(); ?>
<?php $this->endBlock(); ?>
