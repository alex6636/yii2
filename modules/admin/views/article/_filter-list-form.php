<?php
use app\modules\admin\models\article\FilterListModel;
use alexs\yii2crud\controllers\CrudController;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/** @var FilterListModel $FilterListModel */
/** @var CrudController $controller */

$controller = Yii::$app->controller;
$ActiveForm = ActiveForm::begin([
    'action'=>$controller->getRedirectUrl('@action_index'),
    'method'=>'get',
    'enableClientValidation'=>false,
]);
?>
<div class="row">
    <div class="col-md-4">
        <?=$ActiveForm->field($FilterListModel, 'title');?>
    </div>
</div>
<div class="row">
    <div class="col-md-2">
        <?=$ActiveForm->field($FilterListModel, 'public')->radioList([1=>Yii::t('app','Published'), 0=>Yii::t('app','Hidden')])->label(false);?>
    </div>
    <div class="col-md-2">
        <?=Html::submitButton(Yii::t('app','Filter'), ['class'=>'btn btn-md btn-info', 'name'=>'btn-save-form']);?>
        <?=Html::a(Yii::t('app','Reset'), $controller->getRedirectUrl('@action_index'), ['class'=>'btn btn-md btn-success'])?>        
    </div>
</div>
<?php ActiveForm::end(); ?>
