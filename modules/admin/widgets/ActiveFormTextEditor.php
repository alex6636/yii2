<?php
/**
 * @package Admin Panel
 * @class   ActiveFormTextEditor
 * @author  Alex Sergey (createtruesite@gmail.com)
 *
 * WYSIWYG Widget
 */

namespace app\modules\admin\widgets;
use app\modules\admin\models\CrudModel;
use yii\base\Widget;
use yii\bootstrap\ActiveForm;

class ActiveFormTextEditor extends Widget
{
    /** @var ActiveForm $ActiveForm */
    public $ActiveForm;
    /** @var CrudModel $Model */
    public $Model;
    public $attribute = 'text';

    public function run() {
        return $this->render('active-form-text-editor', [
            'ActiveForm'=>$this->ActiveForm,
            'Model'=>$this->Model,
            'attribute'=>$this->attribute,
        ]);
    }
}
