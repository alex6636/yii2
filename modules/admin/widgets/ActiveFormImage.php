<?php
/**
 * @package Admin Panel
 * @class   ActiveFormImage
 * @author  Alex Sergey (createtruesite@gmail.com)
 *
 * Image Upload Widget
 */

namespace app\modules\admin\widgets;
use yii\helpers\Html;

class ActiveFormImage extends ActiveFormFile
{
    public $attribute = 'image';

    protected function getTemplate() {
        $template = '{input}';
        if ($url = $this->Model->getUploadedFileURL($this->attribute)) {
            $template .= '<div class="uploaded-file ' . Html::getInputId($this->Model, $this->attribute) . '">';
            $template .= '<a href="' . $url . '" target="_blank">';
            $template .= Html::img($url);
            $template .= '</a>';
            if (!$this->Model->isAttributeRequired($this->attribute)) {
                $template .= $this->getTemplateDeleteCheckbox();
            }
            $template .= '</div>';
        }
        return $template;
    }
}
