<?php
use app\modules\admin\models\CrudModel;
use yii\bootstrap\ActiveForm;
use yii\widgets\ActiveField;
use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var ActiveForm $ActiveForm */
/** @var CrudModel $Model */
/** @var string $attribute */
/** @var string $template */
/** @var ActiveField $field */

$field = $ActiveForm->field($Model, $attribute, ['inputTemplate'=>$template, 'enableClientValidation'=>false]);
$field->parts['{input}'] = Html::activeHiddenInput($Model, $attribute, ['id'=>NULL]) .
                           Html::activeInput('file', $Model, $attribute);
echo $field;
