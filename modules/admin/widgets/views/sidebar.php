<?php
use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var array $nav */
/** @var string|NULL $active */
?>
<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            <?php if (!empty($nav)) { ?>
                <?php foreach ($nav as $title=>$menu_item) { ?>
                    <li>
                        <?php if (!empty($menu_item['items'])) { ?>
                            <a href="#">
                                <i class="fa <?=$menu_item['fa_icon']?> fa-fw"></i><?=Html::encode($title);?><span class="fa arrow"></span>
                            </a>
                            <ul class="nav nav-second-level">
                                <?php foreach($menu_item['items'] as $_title=>$submenu_item) { ?>
                                    <li>
                                        <a class="<?=(isset($submenu_item['active']) && ($active == $submenu_item['active'])) ? 'active' : ''?>" href="<?=$submenu_item['link']?>"><?=Html::encode($_title);?></a>
                                    </li>
                                <?php } ?>
                            </ul>
                        <?php } else { ?>
                            <a class="<?=(isset($menu_item['active']) && ($active == $menu_item['active'])) ? 'active' : ''?>" href="<?=$menu_item['link']?>">
                                <i class="fa <?=$menu_item['fa_icon']?> fa-fw"></i><?=Html::encode($title);?>
                            </a>
                        <?php } ?>
                    </li>
                <?php } ?>
            <?php } ?>
        </ul>
    </div>
</div>