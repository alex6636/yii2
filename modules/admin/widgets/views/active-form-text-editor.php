<?php
use app\modules\admin\assets\AdminAsset;
use app\modules\admin\models\CrudModel;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var ActiveForm $ActiveForm */
/** @var CrudModel $Model */
/** @var string $attribute */

$this->registerJsFile(
    Url::to(['/resources/plugins/ckeditor/ckeditor.js']),
    ['depends'=>[AdminAsset::class]]
);

echo $ActiveForm->field($Model, $attribute, ['enableClientValidation'=>false])->textarea(['class'=>'form-control ckeditor']);
