<?php
namespace app\modules\admin\widgets;
use yii\base\Widget;

class Navigation extends Widget
{
    public $active = null;

    public function run() {
        /** @noinspection PhpIncludeInspection */
        $nav = require \Yii::getAlias('@admin') . '/config/nav.php';
        return $this->render('sidebar', [
            'nav'=>$nav,
            'active'=>$this->active,
        ]);
    }
}
