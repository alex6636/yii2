<?php
/**
 * @package Admin Panel
 * @class   ActiveFormFile
 * @author  Alex Sergey (createtruesite@gmail.com)
 *
 * File Upload Widget
 */

namespace app\modules\admin\widgets;
use app\modules\admin\models\CrudModel;
use yii\base\Widget;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

class ActiveFormFile extends Widget
{
    /** @var ActiveForm $ActiveForm */
    public $ActiveForm;
    /** @var CrudModel $Model */
    public $Model;
    public $attribute = 'file';

    public function run() {
        return $this->render('active-form-file', [
            'ActiveForm'=>$this->ActiveForm,
            'Model'=>$this->Model,
            'attribute'=>$this->attribute,
            'template'=>$this->getTemplate(),
        ]);
    }

    protected function getTemplate() {
        $template = '{input}';
        if ($url = $this->Model->getUploadedFileURL($this->attribute)) {
            $template .= '<div class="uploaded-file ' . Html::getInputId($this->Model, $this->attribute) . '">';
            $template .= Html::a($this->Model->{$this->attribute}, $url, ['target'=>'_blank']);
            if (!$this->Model->isAttributeRequired($this->attribute)) {
                $template .= $this->getTemplateDeleteCheckbox();
            }
            $template .= '</div>';
        }
        return $template;
    }

    protected function getTemplateDeleteCheckbox() {
        return '<label class="delete">' .
                Html::checkbox(Html::getInputName($this->Model, $this->attribute), false, ['value'=>'delete']) . ' ' . \Yii::t('app', 'Delete') .
               '</label>';
    }
}
