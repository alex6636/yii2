<?php
namespace app\modules\admin\controllers;

class UserController extends CrudController
{
    public function actions() {
        return [
            'add'   =>'app\modules\admin\actions\user\AddAction',
            'edit'  =>'app\modules\admin\actions\user\EditAction',
            'index' =>'app\modules\admin\actions\base\IndexAction',
            'delete'=>'app\modules\admin\actions\base\DeleteAction',
        ];
    }
}
