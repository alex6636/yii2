<?php
namespace app\modules\admin\controllers;

class PageController extends CrudController
{
    public function actions() {
        return [
            'edit'=>'app\modules\admin\actions\base\EditOnlyAction',
        ];
    }
} 
