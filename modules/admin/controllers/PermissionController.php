<?php
namespace app\modules\admin\controllers;
use yii\web\Controller;
use yii\base\Action;

class PermissionController extends Controller
{
    /**
     * @param Action $action
     * @return bool
     */
    public function beforeAction($action) {
        \Yii::setAlias('@action_index',  $this->id . '/index');
        \Yii::setAlias('@action_add',    $this->id . '/add');
        \Yii::setAlias('@action_delete', $this->id . '/delete');
        return parent::beforeAction($action);
    }

    public function actions() {
        $actions_namespace = '\app\modules\admin\actions\permission';
        return array_merge(parent::actions(), [
            'index' =>$actions_namespace . '\IndexAction',
            'add'   =>$actions_namespace . '\AddAction',
            'delete'=>$actions_namespace . '\DeleteAction',
        ]);
    }
}
