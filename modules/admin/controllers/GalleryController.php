<?php
namespace app\modules\admin\controllers;

class GalleryController extends \alexs\alexgallery\controllers\GalleryController
{
    public $enableCsrfValidation = false;

    public function actionIndex() {
        $this->layout = 'form';
        return $this->render('form', [

        ]);
    }
} 
