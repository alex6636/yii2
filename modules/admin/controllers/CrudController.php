<?php
namespace app\modules\admin\controllers;

class CrudController extends \alexs\yii2crud\controllers\CrudController
{
    public $actions_namespace = '\app\modules\admin\actions\base';
}
