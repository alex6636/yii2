<?php
namespace app\modules\admin\controllers;
use app\models\User;
use app\modules\admin\models\LoginModel;
use app\modules\admin\models\ForgotModel;
use Yii;
use yii\web\Controller;
use yii\helpers\Url;

class IndexController extends Controller
{
    public function actionIndex() {
        $User = User::findOne(Yii::$app->user->getId());
        return $this->render('index', [
            'User'=>$User,
        ]);
    }

    public function actionLogin() {
        $LoginModel = new LoginModel;
        if ($LoginModel->load(Yii::$app->request->post()) && $LoginModel->login()) {
            $url = Yii::$app->session->has('admin_redirect_url') ?
                   Yii::$app->session->get('admin_redirect_url') :
                   Url::to(['index']);
            return $this->redirect($url);
        }
        $this->layout = 'login';
        return $this->render('login', [
            'LoginModel'=>$LoginModel,
        ]);
    }

    public function actionLogout() {
        Yii::$app->user->logout();
        $this->redirect(Url::to(['index']));
    }

    public function actionForgot() {
        $ForgotModel = new ForgotModel;
        $ForgotModel->scenario = ForgotModel::SCENARIO_FORGOT;
        if ($ForgotModel->load(Yii::$app->request->post())) {
            if ($ForgotModel->forgot('index/reset-password')) {
                Yii::$app->session->setFlash('message', \Yii::t('app', 'Please check your email'));
                return $this->refresh();                
            }
        }
        $this->layout = 'login';
        return $this->render('forgot', [
            'ForgotModel'=>$ForgotModel,
        ]);
    }

    public function actionResetPassword($forgot_key) {
        $ForgotModel = new ForgotModel;
        $ForgotModel->scenario = ForgotModel::SCENARIO_RESETPASS;
        if ($ForgotModel->load(Yii::$app->request->post())) {
            if ($ForgotModel->reset($forgot_key)) {
                Yii::$app->session->setFlash('message', \Yii::t('app', 'Your password was successfully changed'));
                return $this->refresh();
            }
        }
        $this->layout = 'login';
        return $this->render('reset-password', [
            'ForgotModel'=>$ForgotModel,
        ]);
    }
}
