<?php
namespace app\modules\admin\controllers;

class ArticleController extends CrudController
{
    public function actions() {
        return array_merge(parent::actions(), [
            'index'=>'\app\modules\admin\actions\article\IndexAction',
        ]);
    }
}
